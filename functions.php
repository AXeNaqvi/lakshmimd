<?php
define('AXEVERSION', '1.0.4');
/**
 * Enqueue parent theme stylesheet
 *
 * This runs only if parent theme does not claim support for
 * `child-theme-stylesheet`, and so we need to enqueue this
 * child theme's `style.css` file ourselves.
 *
 * If parent theme supports `child-theme-stylesheet`, it enqueues
 * this child theme's `style.css` file automatically.
 *
 * @since    1.0.0
 * @version  1.0.0
 */
function lakshmimd_parent_theme_style()
{
    if (!current_theme_supports('child-theme-stylesheet')) {
        wp_enqueue_style('lakshmimd-parent-style', get_template_directory_uri() . '/style.css');
        wp_enqueue_style('lakshmimd-child-style', get_stylesheet_uri() . '?ver=' . AXEVERSION);
    }
    wp_enqueue_style('axe-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'lakshmimd_parent_theme_style', 1000);

/**
 * Copy parent theme options (customizer settings)
 *
 * This runs only during child theme activation,
 * and only when there are no child theme options saved.
 *
 * @since    1.0.0
 * @version  1.0.0
 */
function lakshmimd_parent_theme_options()
{
    if (false === get_theme_mods()) {
        $parent_theme_options = get_option('theme_mods_' . get_template());
        update_option('theme_mods_' . get_stylesheet(), $parent_theme_options);
    }
}
add_action('after_switch_theme', 'lakshmimd_parent_theme_options');

/**
 * Put your custom PHP code below...
 */
function axe_after_theme_setup()
{
    add_action('widgets_init', 'lakshmimd_widgets_init');

    add_filter('wmhook_polyclinic_tf_get_theme_mod_output', 'axe_theme_mod_output_filter', 10, 4);
    add_shortcode('axeyear', 'axe_year_shortcode');
    add_shortcode('axeicon', 'axe_icon_shortcode');

    remove_action('tha_header_top', 'polyclinic_header_inner_wrap_open', 110);
    remove_action('tha_header_bottom', 'polyclinic_header_inner_wrap_close', 10);

    add_action('tha_header_top', 'lakshmimd_header_inner_wrap_open', 109);
    add_action('tha_header_bottom', 'lakshmimd_header_inner_wrap_close', 10);
    add_action('tha_header_top', 'lakshmimd_after_logo_info', 116);

    remove_action('tha_footer_bottom', 'polyclinic_footer_credits', 90);
    add_action('tha_footer_bottom', 'lakshmimd_footer_credits', 90);
}
add_action('after_setup_theme', 'axe_after_theme_setup', 10);

function lakshmimd_widgets_init()
{
    register_sidebar(array(
        'name' => 'Logo Section',
        'id' => 'sidebar-lakshmilogo',
        'description' => 'Logo for Lakshmi MD',
        'before_widget' => '<aside id="%1$s" class="widget themeaxe-sidebar sidebar-lakshmilogo %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}

function axe_theme_mod_output_filter($output, $id, $type, $suffix)
{
    switch ($id) {
        case 'texts_credits':
            ob_start();
            echo apply_filters('the_content', $output);
            $output = ob_get_clean();
            break;
    }
    return $output;
}
function axe_icon_shortcode($atts = null)
{
    $atts = extract(shortcode_atts(array(
        'class' => '',
        'echo' => 0,
    ), $atts));
    $content = trim($class) ? sprintf('<i class="%1$s"> </i>', $class) : '';
    if ($echo == '0') {
        return $content;
    } else {
        echo $content;
    }
}
function axe_year_shortcode($atts = null)
{
    $atts = extract(shortcode_atts(array(
        'start' => date('Y'),
        'echo' => 0,
    ), $atts));
    $current = date('Y');
    $content = $current > $start ? sprintf('%1$d - %2$d', $start, $current) : $current;
    if ($echo == '0') {
        return $content;
    } else {
        echo $content;
    }
}

/**
 * Header inner wrap opening
 *
 * @since    1.0
 * @version  1.0
 */
function lakshmimd_header_inner_wrap_open()
{
    ob_start();
    dynamic_sidebar('sidebar-lakshmilogo');
    $logo = ob_get_clean();

    echo "\r\n\r\n" . '<div class="site-header-inner lakshmimd-site-header-inner"><div class="lakshmimdlognav lakshmimd-logo">' . $logo . '</div><div class="lakshmimdlognav lakshmimdnav"><div class="lakshmimdnavinner">' . "\r\n\r\n";
}
/**
 * Header inner wrap closing
 *
 * @since    1.0
 * @version  1.0
 */
function lakshmimd_header_inner_wrap_close()
{
    echo "\r\n\r\n" . '</div></div>' . "\r\n\r\n";
}

function lakshmimd_after_logo_info()
{
    echo '</div>';
}
/**
 * Footer credits
 *
 * @since    1.0.0
 * @version  1.0.0
 * @overridden  1.9.3
 */
function lakshmimd_footer_credits()
{

    /* Helper variables */

    $footer_credits = trim(Polyclinic_Theme_Framework::get_theme_mod('texts_credits'));

    if (empty($footer_credits)) {

        /* Default credits text */

        $footer_credits = '&copy; ' . date('Y');
        $footer_credits .= ' ';
        $footer_credits .= '<a href="' . esc_url(home_url('/')) . '">' . get_bloginfo('name') . '</a>';
        $footer_credits .= '<span class="sep"> | </span>';
        $footer_credits .= sprintf(
            esc_html_x('Using %1$s %2$s theme created by %3$s', '1: theme name, 2: linked "WordPress" word, 3: theme developer name.', 'polyclinic'),
            '<a rel="nofollow" href="' . esc_url(wp_get_theme('polyclinic')->get('ThemeURI')) . '"><strong>' . wp_get_theme('polyclinic')->get('Name') . '</strong></a>',
            '<a rel="nofollow" href="' . esc_url(__('http://wordpress.org/', 'polyclinic')) . '">WordPress</a>',
            '<a href="https://www.webmandesign.eu">WebMan Design</a>'
        );

        if (function_exists('get_the_privacy_policy_link')) {
            $footer_credits .= get_the_privacy_policy_link('<span class="sep"> | </span>');
        }

        $footer_credits .= '<span class="sep"> | </span>';
        $footer_credits .= '<a href="#" id="back-to-top" class="back-to-top">' . esc_html__('Back to top &uarr;', 'polyclinic') . '</a>';
    }

    /* Output */

    if ('-' !== $footer_credits) {

        echo '<div class="site-footer-area footer-area-site-info">';
        echo '<div class="site-footer-area-inner site-info-inner">';
        /* No need to apply wp_kses_post() on output as it is already validated via Customizer. */
        echo '<div id="site-info" class="site-info">' . (string) $footer_credits . '</div>';
        get_template_part('template-parts/axemenu', 'social');
        echo '</div>';
        echo '</div>';
    }

}
function lakshmimd_social_menu_args($items_wrap = '<ul data-id="%1$s" class="%2$s">%3$s</ul>')
{
    return array(
        'menu' => 'axesocial',
        'container' => false,
        'menu_class' => 'social-links-items',
        'depth' => 1,
        'link_before' => '<span class="screen-reader-text">',
        'link_after' => '</span><!--{{icon}}-->',
        'fallback_cb' => false,
        'items_wrap' => (string) $items_wrap,
    );
}
